<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'variable'  => 'title',
                'type'      => 'text',
                'value'     => 'Siskasaji Digital'
            ],
            [
                'variable'  => 'description',
                'type'      => 'text',
                'value'     => 'Sebuah blablabla..'
            ],
            [
                'variable'  => 'favicon',
                'type'      => 'file',
                'value'     => 'favicon.ico'
            ],
        ];

        foreach($settings as $setting) {
            Setting::create($setting);
        }
    }
}

<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\Models\Post;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public static function userLogin() {
        return auth()->user()->id;
    }

    public static function checkOldPassword($current_password) {
        $account = self::findOrFail(self::userLogin());

        return Hash::check($current_password, $account->password);
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }
}

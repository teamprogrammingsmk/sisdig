<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    public static function settings() {
        $setting = new self;
        $setting->title = self::title();
        $setting->description = self::description();
        $setting->favicon = self::favicon();
        $setting->appLink = self::appLink();

        return $setting;
    }

    public static function title() {
        $setting = self::where('variable', 'title')->first();
        
        return $setting->value;
    }

    public static function description() {
        $setting = self::where('variable', 'description')->first();
        
        return $setting->value;
    }

    public static function favicon() {
        $setting = self::where('variable', 'favicon')->first();
        
        return $setting->value;
    }
}

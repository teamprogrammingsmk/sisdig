<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User, App\Models\Category;

class Post extends Model
{
    use SoftDeletes;

    protected $guarded = ['deleted_at'];
    protected $hidden = ['deleted_at'];

    // All Articles
    public static function getArticles()
    {
        $query = self::with('categories', 'user');

        return $query->latest();
    }

    // Single Article
    public static function getArticle($id)
    {
        $query = self::where('id', $id)
            ->where('published', 1)->first();
        $addViewer = $query->update([
            'viewed' => $query->viewed + 1
        ]);

        return $query;
    }

    // Counted Data
    public static function countedData()
    {
        $data = new self;
        $data->all = self::countArticles();
        $data->draft = self::countDraftArticles();
        $data->published = self::countPublishedArticles();
        $data->trash = self::countTrashedArticles();

        return $data;
    }

    public static function countArticles()
    {
        $query = self::count();

        return $query;
    }

    public static function countDraftArticles()
    {
        $query = self::where('published', 0)
            ->count();

        return $query;
    }

    public static function countPublishedArticles()
    {
        $query = self::where('published', 1)
            ->count();

        return $query;
    }

    public static function countTrashedArticles()
    {
        $query = self::onlyTrashed();

        return $query->count();
    }

    // Trashed
    public static function getTrashedArticles()
    {
        $query = self::with('categories', 'user')
            ->onlyTrashed();

        return $query->latest();
    }

    // Published
    public static function getPublishedArticles()
    {
        $query = self::with('categories', 'user')
            ->where('published',  1);

        return $query->latest();
    }

    // Popular
    public static function getPopularArticles($limit = NULL)
    {
        $query = self::with('categories', 'user')
            ->where('published', 1)
            ->orderBy('viewed', 'desc');

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query->get();
    }

    // Recomendation
    public static function getRandomArticles()
    {
        $query = self::with('categories', 'user')
            ->where('published', 1)
            ->limit(10)
            ->inRandomOrder();

        return $query->get();
    }

    // Draft
    public static function getDraftArticles()
    {
        $query = self::with('categories', 'user')
            ->where('published',  0);

        return $query->latest();
    }

    // Search
    public static function search($query)
    {
        $query = self::with('categories', 'user')
            ->where('title', 'LIKE', "%" . $query . "%")
            ->where('published',  1);

        return $query->latest();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsTo(Category::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class Category extends Model
{
    protected $guarded = [];

    public static function getCategories()
    {
        $query = self::orderBy('title', 'ASC')->get();

        return $query;
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request, Illuminate\Support\Facades\DB, Illuminate\Support\Str;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $models = Category::getCategories();
            return DataTables::of($models)
                ->addColumn('title', function ($models) {
                    $links = "<a href=" . route('admin.category.edit', $models->id) . ">$models->title</a>";

                    return $links;
                })
                ->addColumn('action', function ($models) {
                    $button = "<a href=" . route('admin.category.edit', $models->id) . " class='btn btn-outline-primary btn-sm'>Edit</a>";
                    if ($models->id != 1) {
                        $button .= "&nbsp;&nbsp;";
                        $button .= "<form action=" . route('admin.category.destroy', $models->id) . " method='post' class='d-inline'>" . csrf_field() . method_field('delete') . "<button  class='btn btn-danger btn-sm text-white' type='submit'>Delete</button></form>";
                    }

                    return $button;
                })
                ->rawColumns(['title', 'action'])
                ->make(true);
        }

        return view('category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        DB::beginTransaction();

        try {
            Category::create([
                'title' => $request->title,
                'slug'  => Str::lower(str_replace(' ', '-', $request->title)),
                'image' => $request->image
            ]);

            DB::commit();
            return redirect(route('admin.category.index'))->with('success', 'Thank You for your subscription');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect(route('admin.category.index'))->with('warning', 'Something Went Wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        DB::beginTransaction();

        try {
            Category::findOrFail($id)
                ->update([
                    'title' => $request->title,
                    'slug'  => Str::lower(str_replace(' ', '-', $request->title)),
                    'image' => $request->image
                ]);

            DB::commit();
            return redirect(route('admin.category.index'))->with('success', 'Thank You for your subscription');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect(route('admin.category.index'))->with('warning', 'Something Went Wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::destroy($id);

        return redirect(route('admin.category.index'))->with('success', 'Thank You for your subscription');
    }
}

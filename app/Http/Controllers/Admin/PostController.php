<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request, Illuminate\Support\Facades\DB;
use App\Http\Requests\PostRequest;
use App\Models\Post, App\Models\Category;
use DataTables, \Carbon\Carbon;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Post::countedData();

        if($request->ajax()) {
            $models = Post::getArticles();
            return DataTables::of($models)
                            ->addColumn('title', function($models) {
                                $links = "<a href=".route('admin.article.edit', $models->id).">$models->title</a>";
                                if($models->published == 0) {
                                    $links .= "<strong class='text-danger ml-2'>[Draft]</strong>";
                                }

                                return $links;
                            })
                            ->addColumn('author', function($models) {
                                return $models->user->name;
                            })
                            ->addColumn('category', function($models) {
                                return $models->categories->title;
                            })
                            ->addColumn('date', function($models) {
                                $date = Carbon::parse($models->update_at)->translatedFormat('d/m/Y');
                                $date .= ' at ';
                                $date .= Carbon::parse($models->update_at)->format('H:i A');

                                return $date;
                            })
                            ->addColumn('action', function($models) {
                                $button = "<a href=".route('admin.article.edit', $models->id)." class='btn btn-outline-primary btn-sm'>Edit</a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<form action=".route('admin.article.trash', $models->id)." method='post' class='d-inline'>".csrf_field()."<button  class='btn btn-outline-danger btn-sm' type='submit'>Trash</button></form>";

                                return $button;
                            })
                            ->rawColumns(['title', 'author', 'category', 'date', 'action'])
                            ->make(true);
        }
        
        return view('articles.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::getCategories();

        return view('articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        DB::beginTransaction();

        try{
            Post::create([
                    'user_id'       => auth()->user()->id,
                    'categories_id' => $request->category,
                    'title'         => $request->title,
                    'thumb'         => $request->thumb,
                    'article'       => $request->editor,
                    'published'     => $request->publish == 1 ? 1 : 0
                ]);

            DB::commit();
            return redirect(route('admin.article.index'))->with('success','Thank You for your subscription');
        } catch (\Exception $e){
            DB::rollback();
            return redirect(route('admin.article.index'))->with('warning','Something Went Wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the listening draft item.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function draft(Request $request)
    {
        $data = Post::countedData();
        
        if($request->ajax()) {
            $models = Post::getDraftArticles();
            return DataTables::of($models)
                            ->addColumn('title', function($models) {
                                $links = "<a href=".route('admin.article.edit', $models->id).">$models->title</a>";

                                return $links;
                            })
                            ->addColumn('author', function($models) {
                                return $models->user->name;
                            })
                            ->addColumn('category', function($models) {
                                return $models->categories->title;
                            })
                            ->addColumn('date', function($models) {
                                $date = Carbon::parse($models->update_at)->translatedFormat('d/m/Y');
                                $date .= ' at ';
                                $date .= Carbon::parse($models->update_at)->format('H:i A');

                                return $date;
                            })
                            ->addColumn('action', function($models) {
                                $button = "<a href=".route('admin.article.edit', $models->id)." class='btn btn-outline-primary btn-sm'>Edit</a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<form action=".route('admin.article.trash', $models->id)." method='post' class='d-inline'>".csrf_field()."<button  class='btn btn-outline-danger btn-sm' type='submit'>Trash</button></form>";

                                return $button;
                            })
                            ->rawColumns(['title', 'author', 'category', 'date', 'action'])
                            ->make(true);
        }
        
        return view('articles.draft', compact('data'));
    }

    /**
     * Display the listening published item.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function published(Request $request)
    {
        $data = Post::countedData();
        
        if($request->ajax()) {
            $models = Post::getPublishedArticles();
            return DataTables::of($models)
                            ->addColumn('title', function($models) {
                                $links = "<a href=".route('admin.article.edit', $models->id).">$models->title</a>";

                                return $links;
                            })
                            ->addColumn('author', function($models) {
                                return $models->user->name;
                            })
                            ->addColumn('category', function($models) {
                                return $models->categories->title;
                            })
                            ->addColumn('date', function($models) {
                                $date = Carbon::parse($models->update_at)->translatedFormat('d/m/Y');
                                $date .= ' at ';
                                $date .= Carbon::parse($models->update_at)->format('H:i A');

                                return $date;
                            })
                            ->addColumn('action', function($models) {
                                $button = "<a href=".route('admin.article.edit', $models->id)." class='btn btn-outline-primary btn-sm'>Edit</a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<form action=".route('admin.article.trash', $models->id)." method='post' class='d-inline'>".csrf_field()."<button  class='btn btn-outline-danger btn-sm' type='submit'>Trash</button></form>";

                                return $button;
                            })
                            ->rawColumns(['title', 'author', 'category', 'date', 'action'])
                            ->make(true);
        }
        
        return view('articles.published', compact('data'));
    }

    /**
     * Display the listening trashed item.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $data = Post::countedData();
        
        if($request->ajax()) {
            $models = Post::getTrashedArticles();
            return DataTables::of($models)
                            ->addColumn('title', function($models) {
                                $links = "<a href=".route('admin.article.edit', $models->id).">$models->title</a>";

                                return $links;
                            })
                            ->addColumn('author', function($models) {
                                return $models->user->name;
                            })
                            ->addColumn('category', function($models) {
                                return $models->categories->title;
                            })
                            ->addColumn('date', function($models) {
                                $date = Carbon::parse($models->update_at)->translatedFormat('d/m/Y');
                                $date .= ' at ';
                                $date .= Carbon::parse($models->update_at)->format('H:i A');

                                return $date;
                            })
                            ->addColumn('action', function($models) {
                                $button = "<form action=".route('admin.article.restore', $models->id)." method='post' class='d-inline'>".csrf_field()."<button  class='btn btn-outline-primary btn-sm' type='submit'>Restore</button></form>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<form action=".route('admin.article.destroy', $models->id)." method='post' class='d-inline'>".csrf_field() . method_field('delete') ."<button  class='btn btn-outline-danger btn-sm' type='submit'>Delete Permanently</button></form>";

                                return $button;
                            })
                            ->rawColumns(['title', 'author', 'category', 'date', 'action'])
                            ->make(true);
        }
        
        return view('articles.trashed', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Post::findOrFail($id);
        $categories = Category::get();

        return view('articles.edit', compact('article', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        DB::beginTransaction();

        try{
            Post::findOrFail($id)
                ->update([
                    'categories_id' => $request->category,
                    'title'         => $request->title,
                    'thumb'         => $request->thumb,
                    'article'       => $request->editor,
                    'published'     => $request->publish == 1 ? 1 : 0
                ]);

            DB::commit();
            return redirect(route('admin.article.index'))->with('success','Thank You for your subscription');
        } catch (\Exception $e){
            DB::rollback();
            return redirect(route('admin.article.index'))->with('warning','Something Went Wrong!');
        }
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function trash($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        
        return redirect(route('admin.article.index'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $post = Post::withTrashed()->findOrFail($id);
        $post->restore();
        
        return redirect(route('admin.article.index'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::withTrashed()->findOrFail($id);
        $post->forceDelete();
        
        return redirect(route('admin.article.index'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request, Illuminate\Support\Facades\DB;;
use App\Http\Requests\SettingRequest;
use App\Models\Setting;

class SettingController extends Controller
{
    public function index() {
        $settings = Setting::get();

        return view('settings.index', compact('settings'));
    }

    public function update(SettingRequest $request) {
        DB::beginTransaction();

        try{
            $updates = $request->all();
            foreach($updates as $key => $value) {
                Setting::where('variable', $key)->update(['value' => $value]);
            }

            DB::commit();
            return redirect(route('admin.settings.index'))->with('success','Thank You for your subscription');
        } catch (\Exception $e){
            DB::rollback();
            return redirect(route('admin.settings.index'))->with('warning','Something Went Wrong!');
        }
    }
}

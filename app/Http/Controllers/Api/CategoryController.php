<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::getCategories();

        return response()->json([
            'status'    => true,
            'data'      => $categories
        ], 200);
    }

    public function show($id)
    {
        $categories = Category::findOrFail($id);
        $posts = Post::where('categories_id', $id)->latest()->paginate(10);

        return response()->json([
            'status'        => true,
            'data'          => [
                'category' => $categories,
                'posts'      => $posts
            ]
        ], 200);
    }
}

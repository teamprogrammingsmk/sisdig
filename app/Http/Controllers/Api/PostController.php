<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use \Carbon\Carbon;

class PostController extends Controller
{
    // All
    public function index()
    {
        $posts = Post::getPublishedArticles()->paginate(10);

        return response()->json([
            'status'    => true,
            'data'      => $posts
        ], 200);
    }

    // Recomendation
    public function recomendation()
    {
        $posts = Post::getRandomArticles();

        return response()->json([
            'status'    => true,
            'data'      => $posts
        ], 200);
    }

    // Popular
    public function popular()
    {
        $posts = Post::getPopularArticles();

        return response()->json([
            'status'    => true,
            'data'      => $posts
        ], 200);
    }

    // Search
    public function search(Request $request)
    {
        $posts = Post::search($request->get('query'))->paginate(10);

        return response()->json([
            'status'    => true,
            'data'      => $posts
        ], 200);
    }

    // Single Article
    public function show($id)
    {
        $post = Post::getArticle($id);

        if ($post) {
            return response()->json([
                'status'    => true,
                'data'      => $post
            ], 200);
        } else {
            return response()->json([
                'status'    => false,
                'data'      => [
                    'message'   => 'Artikel tidak ditemukan'
                ]
            ], 404);
        }
    }
}

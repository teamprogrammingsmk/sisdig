<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserProfileRequest;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        return view('account.index');
    }

    public function profileUpdate(UserProfileRequest $request) {
        if(User::checkOldPassword($request->current_password)) {
            try {
                User::findOrFail(User::userLogin())
                ->update([
                    'name'  => $request->name,
                    'email' => $request->email
                ]);

                
                return redirect(route('account.index'))->with('success', 'Thank You for your subscription');
            } catch(\Exception $e) {
                return redirect(route('account.index'))->with('error', 'Something Went Wrong!');
            }
        } else {
            return redirect(route('account.index'))->with('success', 'Perubahan tersimpan');
        }
    }
}

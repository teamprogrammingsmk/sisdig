<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    // Posts
    Route::resource('posts', 'PostController', ['except' => ['create', 'edit', 'store', 'update', 'destroy']]);
    Route::get('search', 'PostController@search');
    Route::get('popular', 'PostController@popular');
    Route::get('recomendation', 'PostController@recomendation');

    // Categories
    Route::get('categories', 'CategoryController@index');
    Route::get('categories/{id}', 'CategoryController@show');
});

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@index');

Route::get('/home', function() {
    return redirect(route('admin.home'));
});

Auth::routes(['register' => false]);

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function() {
    Route::name('admin.')->group(function () {
        Route::get('home', 'HomeController@index')->name('home');
        Route::resource('category', 'CategoryController', ['except' => ['create', 'show']]);
        
        // Article
        Route::resource('article', 'PostController', ['except' => ['show']]);
        Route::get('article/trash', 'PostController@trashed')->name('article.trashed');
        Route::get('article/published', 'PostController@published')->name('article.published');
        Route::get('article/draft', 'PostController@draft')->name('article.draft');
        Route::post('article/{id}/trash', 'PostController@trash')->name('article.trash');
        Route::post('article/{id}/restore', 'PostController@restore')->name('article.restore');

        // Settings
        Route::get('settings', 'SettingController@index')->name('settings.index');
        Route::put('settings', 'SettingController@update')->name('settings.update');
    });
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('account', 'UserController@index')->name('account.index');
    Route::put('account', 'UserController@profileUpdate')->name('account.edit-profile');
});
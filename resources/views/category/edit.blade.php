@extends('layouts.app')

@section('title', 'Ubah Kategori')

@section('content')
<div class="row justify-content-center">
    <div class="col-12">
        <div class="row align-items-center mb-2">
            <div class="col">
                <h2 class="h5 page-title">@yield('title')</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card shadow mb-4">
                    <div class="card-body px-4">
                        <form action="{{ route('admin.category.update', $category->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="title" class="col-form-label">Judul</label>
                                <input type="text" name="title" id="title"
                                    class="form-control @error('title') is-invalid @enderror"
                                    value="{{ $category->title }}">

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="slug" class="col-form-label">Slug</label>
                                <input type="text" name="slug" id="slug" class="form-control"
                                    value="{{ $category->slug }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-form-label">Image</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="image" name="image"
                                        value="{{ $category->image }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text"
                                            onclick="filemanager.selectFile('image')">Pilih</span>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-sm btn-outline-primary float-right fe fe-send fe-16"> Submit</button>
                        </form>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#title').keyup(function () {
            $('#slug').val($(this).val().replace(/ /g, '-'));
        });
    });

</script>
@endsection

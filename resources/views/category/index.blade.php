@extends('layouts.app')

@section('title', 'Kategori')

@section('content')
<div class="row justify-content-center">
    <div class="col-12">
        <div class="row align-items-center mb-2">
            <div class="col">
                <h2 class="h5 page-title">@yield('title')</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-header">
                        <strong class="card-title">Tambah baru</strong>
                    </div>
                    <div class="card-body px-4">
                        <form action="{{ route('admin.category.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="title">Judul</label>
                                <input type="text" name="title" id="title"
                                    class="form-control @error('title') is-invalid @enderror">

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="image" name="image" value="{{ old('image') }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" onclick="filemanager.selectFile('image')">Pilih</span>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-sm btn-outline-primary float-right fe fe-send fe-16"> Submit</button>
                        </form>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div>
            <div class="col-md-8">
                <div class="card shadow mb-4">
                    <div class="card-body px-4">
                        <div class="table-responsive">
                            <table id="table" class="table table-striped table-borderless mb-1 mx-n1 table-sm">
                                <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Slug</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col -->
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{{ route('admin.category.index') }}"
            },
            responsive: true,
            "columns": [{
                    data: 'title',
                    name: 'title'
                },
                {
                    data: 'slug',
                    name: 'slug',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        })
    })

</script>
@endsection

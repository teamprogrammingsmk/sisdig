<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('components.head')
</head>

<body class="vertical light">
    <div class="wrapper">
        @include('components.navbar')
        @include('components.sidebar')
        <main role="main" class="main-content">
            <div class="container-fluid">
                @yield('content')
            </div> <!-- .container-fluid -->
        </main> <!-- main -->
    </div> <!-- .wrapper -->
    @include('components.scripts')
    @include('sweetalert::alert')
</body>

</html>

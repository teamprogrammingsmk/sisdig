<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('components.head')
</head>

<body class="light">
    <div class="container vh-100">
        <div class="row align-items-center h-100">
            @yield('content')
        </div>
    </div>
    @include('components.scripts')
</body>

</html>

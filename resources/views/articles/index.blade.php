@extends('layouts.app')

@section('title', 'Artikel')

@section('content')
<div class="row justify-content-center">
    <div class="col-12">
        <div class="row align-items-center mb-2">
            <div class="col">
                <h2 class="h5 page-title">@yield('title')</h2>
                <a href="{{ route('admin.article.create') }}" class="btn btn-sm btn-outline-primary ml-auto"><span class="fe fe-plus fe-16 mr-2"></span>Tambah Baru</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header">
                        @if($data->trash)
                            <a class="float-right ml-2" href="{{ route('admin.article.trashed') }}">Trash ({{ $data->trash }})</a>
                        @endif
                        @if($data->published)
                            <a class="float-right ml-2" href="{{ route('admin.article.published') }}">Published ({{ $data->published }})</a>
                        @endif
                        @if($data->draft)
                            <a class="float-right ml-2" href="{{ route('admin.article.draft') }}">Draft ({{ $data->draft }})</a>
                        @endif
                        <a class="float-right" href="{{ route('admin.article.index') }}">All ({{ $data->all }})</a>
                    </div>
                    <div class="card-body px-4">
                        <div class="table-responsive">
                            <table id="table" class="table table-striped table-borderless mb-1 mx-n1 table-sm">
                                <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Author</th>
                                        <th>Kategori</th>
                                        <th>Tanggal</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col -->
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{{ route('admin.article.index') }}"
            },
            responsive: true,
            "columns": [
                {
                    data: 'title',
                    name: 'title'
                },
                {
                    data: 'user.name',
                    name: 'user.name',
                    orderable: false
                },
                {
                    data: 'categories.title',
                    name: 'categories.title',
                    orderable: false
                },
                {
                    data: 'date',
                    name: 'date',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        })
    })
</script>
@endsection
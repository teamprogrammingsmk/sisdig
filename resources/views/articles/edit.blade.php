@extends('layouts.app')

@section('title', 'Ubah Artikel')

@section('content')
<div class="row justify-content-center">
    <div class="col-12">
        <div class="row align-items-center mb-2">
            <div class="col">
                <h2 class="h5 page-title">@yield('title')</h2>
            </div>
        </div>
        <form action="{{ route('admin.article.update', $article->id) }}" method="post">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ $article->title }}" placeholder="Ubah judul">
                        
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea name="editor" id="editor" class="form-control editor @error('editor') is-invalid @enderror">{{ $article->article }}</textarea>
                    
                        @error('editor')
                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div> <!-- .col -->
                <div class="col-md-4">
                    <div class="card mt-2">
                        <div class="card-header">Thumbnail Image</div>
                        <div class="card-body">
                            <input type="hidden" id="thumb" name="thumb" value="{{ $article->thumb }}">
                            <img src="{{ $article->thumb }}" id="thumb-preview" style="width: 100%;">
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-sm btn-outline-primary" onclick="filemanager.selectFile('thumb')">Set featured image</button>
                            <button type="button" class="btn btn-sm btn-outline-danger float-right" onclick="remove();"> Remove featured image</button>
                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-header">Kategori</div>
                        <div class="card-body">
                            <div class="form-group" style="margin: 0;">
                                <select name="category" id="category" class="form-control select2 @error('category') is-invalid @enderror">
                                    <option value="">Select</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" @if($category->id == $article->categories_id) selected @endif>{{ $category->title }}</option>
                                    @endforeach
                                </select>

                                @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-header">Publish</div>
                        <div class="card-body">
                            <p style="margin: 0;"><span class="fe fe-key fe-16"></span> Status : <strong>@if($article->published == 0) Draft @else Published @endif</strong></p>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="publish" value="0" class="btn btn-sm btn-outline-danger fe fe-save fe-16"> Save Draft</button>
                            <button type="submit" name="publish" value="1" class="btn btn-sm btn-outline-primary float-right fe fe-send fe-16"> Publish</button>
                        </div>
                    </div>
                </div>
            </div> <!-- .row -->
        </form>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	window.onload = function () {
        $('.select2').select2();
		CKEDITOR.replace('editor', {
	        filebrowserBrowseUrl: filemanager.ckBrowseUrl,
	    });
	}

    function remove() {
        let thumb = document.getElementById('thumb')
        let thumbPrev = document.getElementById('thumb-preview')

        thumb.value = ''
        thumbPrev.src = ''
    }
</script>
@endsection
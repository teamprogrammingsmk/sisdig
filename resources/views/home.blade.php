@extends('layouts.app')

@section('title', 'Beranda')

@section('content')
<div class="row justify-content-center">
    <div class="col-12">
        <div class="row align-items-center mb-2">
            <div class="col">
                <h2 class="h5 page-title">@yield('title')</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card shadow mb-4">
                    <div class="card-header">
                        <strong>Post</strong>
                    </div>
                    <div class="card-body px-4">
                        <div class="row">
                            <div class="col-4 text-center mb-3">
                                <p class="mb-1 small text-muted">Published</p>
                                <span class="h3">{{ $counting->published }}</span><br />
                            </div>
                            <div class="col-4 text-center mb-3">
                                <p class="mb-1 small text-muted">Draft</p>
                                <span class="h3">{{ $counting->draft }}</span><br />
                            </div>
                            <div class="col-4 text-center mb-3">
                                <p class="mb-1 small text-muted">Trash</p>
                                <span class="h3">{{ $counting->trash }}</span><br />
                            </div>
                        </div>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col -->
            <div class="col-md-6">
                <div class="card shadow mb-4">
                    <div class="card-header">
                        <strong class="card-title">Artikel Populer</strong>
                        <a class="float-right small text-muted" href="{{ route('admin.article.index') }}">Lihat semua</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless table-sm">
                            <thead>
                                <tr>
                                    <th class="w-50">Judul</th>
                                    <th class="text-right">View</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($popularity->count() < 1)
                                <tr>
                                    <td class="text-center" colspan="2">Tidak ada data</td>
                                </tr>
                                @else
                                    @foreach ($popularity as $popular)
                                    <tr>
                                        <td><a href="{{ route('admin.article.edit', $popular->id) }}">{{ $popular->title }}</a></td>
                                        <td class="text-right">{{ $popular->viewed }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div> <!-- / .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col -->
        </div>
    </div>
</div>
@endsection

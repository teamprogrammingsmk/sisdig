<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ App\Models\Setting::title() }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ App\Models\Setting::description() }}" />
    <meta name="keywords" content="{{ App\Models\Setting::title() }}" />
    <meta content="{{ App\Models\Setting::title() }}" name="author" />
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ App\Models\Setting::favicon() }}">

    <!--Material Icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('landing/css/materialdesignicons.min.css') }}" />

    <!-- Pixeden Icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('landing/css/pe-icon-7.css') }}" />

    <!--Slider-->
    <link rel="stylesheet" href="{{ asset('landing/css/owl.carousel.css') }}" />
    <link rel="stylesheet" href="{{ asset('landing/css/owl.theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('landing/css/owl.transitions.css') }}" />

    <!-- css -->
    <link href="{{ asset('landing/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/css/style.css') }}" rel="stylesheet" type="text/css" />

</head>

<body>
<!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
    </div>

    <!--Navbar Start-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom navbar-light sticky sticky-dark">
        <div class="container">
            <!-- LOGO -->
            <a class="navbar-brand logo" href="{{ url('/') }}">
                <span class="text-white text-uppercase">{{ App\Models\Setting::title() }}</span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="#home" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#about" class="nav-link">About</a>
                    </li>
                    <li class="nav-item">
                        <a href="#services" class="nav-link">Services</a>
                    </li>
                    <li class="nav-item">
                        <a href="#features" class="nav-link">Features</a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <!-- Navbar End -->

    <!-- HOME START -->
    <section class="section home-2-bg" id="home">
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5">
                            <div class="mt-40 home-2-content">
                                <h1 class="text-white font-weight-normal home-2-title display-4 mb-0">Membaca Jadi Lebih Seru</h1>
                                {{-- Reading Has Never Been This More Fun --}}
                                <p class="text-white-70 mt-4 f-15 mb-0">Dengan Siskasaji Digital anda bisa mendapatkan perasaan membaca yang berbeda dari sebelumnya dan tentunya ramah bagi semua kalangan.</p>
                                {{-- With Siskasaji Digital you can get the new experience and feeling of reading like never before and also friendly to all ages--}}
                                <div class="mt-5">
                                    <a href="#" class="btn btn-custom mr-4 rounded">Download</a>
                                </div>
                            </div>
                        </div>
                        <!-- col end -->

                        <div class="col-lg-7">
                            <div class="mt-40 home-2-content position-relative">
                                <img src="{{ asset('landing/images/home-2-img.png') }}" alt="" class="img-fluid mx-auto d-block home-2-img mover-img">
                                <div class="home-2-bottom-img">
                                    <img src="{{ asset('landing/images/homr-2-bg-bottom.png"') }} alt="" class="img-fluid d-block mx-auto">
                                </div>
                            </div>
                        </div>
                        <!-- col end -->
                    </div>
                    <!-- row end -->
                </div>
                <!-- container end -->
            </div>
        </div>
    </section>
    <!-- HOME END -->

    <!-- ABOUT START -->
    <section class="section bg-about bg-light-about bg-light" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-heading mb-5">
                        <h3 class="text-dark mb-1 font-weight-light text-uppercase">About Us</h3>
                        <div class="title-border-simple position-relative"></div>
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->

            <div class="row">
                <div class="col-lg-4">
                    <div class="about-box about-light text-center p-3">
                        <div class="about-icon mb-4">
                            <i class="mdi mdi-lightbulb"></i>
                        </div>
                        <h4 class="font-weight-light"><a href="#" class="text-dark">Desain Yang Kreatif</a></h4>
                        {{-- Creative Design --}}
                        <p class="text-muted f-14">Kami menggunakan desain yang mampu menarik perhatian para pembaca dan juga nyaman untuk dipandang.</p>
                        {{-- We use design that can attract people to read more magazine and also comfortable for the eyes--}}
                    </div>
                </div>
                <!-- col end -->

                <div class="col-lg-4">
                    <div class="about-box about-light text-center p-3">
                        <div class="about-icon mb-4">
                            <i class="mdi mdi-projector-screen"></i>
                        </div>
                        <h4 class="font-weight-light"><a href="#" class="text-dark">Fitur Yang Menarik</a></h4>
                        {{-- Interesting Feature --}}
                        <p class="text-muted f-14">Kami menyuguhkan fitur - fitur yang mampu membuat para membaca mendapat kenyamanan dan pengalaman membaca yang lebih baik.</p>
                        {{-- We provide features that can make reading comfortable and having better experience--}}
                    </div>
                </div>
                <!-- col end -->

                <div class="col-lg-4">
                    <div class="about-box about-light text-center p-3">
                        <div class="about-icon mb-4">
                            <i class="mdi mdi-nature"></i>
                        </div>
                        <h4 class="font-weight-light"><a href="#" class="text-dark"></a></h4>
                        <p class="text-muted f-14">Nemo enim ipsam voluptatem quia voluptas sit aspernatur at aut odit aut fugit sed quia consequuntur magni.</p>
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->

            <div class="row align-items-center mt-5">
                <div class="col-md-6">
                    <div class="about-desc">
                        <h3 class="text-dark mb-3 font-weight-light">Performancect Solution For Small Businesses</h3>
                        <p class="text-muted f-15">Temporibus autem quibusdam a aut officiis debitis rerum necessitatibus saepeeveniet ut et voluptates repudiandae sint a molestiae recusandae itaque earum rerum hic tenetur a sapiente delectus ut at aut reiciendis voluptatibus maiores alias consequatur perferendis doloribus asperiores rerum necessitat saepeeveniet.</p>
                        <div class="about-by">
                            <p class="font-weight-light mb-0"><a href="#" class="text-dark"><i class="mdi mdi-circle-medium text-custom mr-2"></i>
                                    Learn More <span class="text-custom"> About Us</span></a></p>
                        </div>
                    </div>
                </div>
                <!-- col end -->

                <div class="col-md-6">
                    <div class="about-img light-img position-relative p-4">
                        <img src="{{ asset('landing/images/about-img.jpg') }}" alt="" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- ABOUT END -->

    <!-- SERVICE START -->
    <section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-heading mb-5">
                        <h3 class="text-dark mb-1 font-weight-light text-uppercase">Our Services</h3>
                        <div class="title-border-simple position-relative"></div>
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="service-box rounded mt-4 p-4">
                        <div class="service-icon mb-3">
                            <i class="mdi mdi-database"></i>
                        </div>
                        <div class="services-desc">
                            <div class="service-title mb-2 position-relative">
                                <h5 class="font-weight-normal mb-3"><a href="#" class="text-dark">Awesome Support</a></h5>
                            </div>
                            <p class="text-muted mb-3 f-14">Sed ut perspiciatis unde sit omnise iste natus voluptatem site accusantium doloremque laudantium totam.</p>
                            <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">learn more<i class="mdi mdi-arrow-right ml-2"></i></a></p>
                        </div>
                    </div>
                </div>
                <!-- col end -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-box rounded mt-4 p-4">
                        <div class="service-icon mb-3">
                            <i class="mdi mdi-palette"></i>
                        </div>
                        <div class="services-desc">
                            <div class="service-title mb-2 position-relative">
                                <h5 class="font-weight-normal mb-3"><a href="#" class="text-dark">Unlimited Colors</a></h5>
                            </div>
                            <p class="text-muted mb-3 f-14">Sed ut perspiciatis unde sit omnise iste natus voluptatem site accusantium doloremque laudantium totam.</p>
                            <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">learn more<i class="mdi mdi-arrow-right ml-2"></i></a></p>
                        </div>
                    </div>
                </div>
                <!-- col end -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-box rounded mt-4 p-4">
                        <div class="service-icon mb-3">
                            <i class="mdi mdi-finance"></i>
                        </div>
                        <div class="services-desc">
                            <div class="service-title mb-2 position-relative">
                                <h5 class="font-weight-normal mb-3"><a href="#" class="text-dark">Strategy Solutions</a></h5>
                            </div>
                            <p class="text-muted mb-3 f-14">Sed ut perspiciatis unde sit omnise iste natus voluptatem site accusantium doloremque laudantium totam.</p>
                            <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">learn more<i class="mdi mdi-arrow-right ml-2"></i></a></p>
                        </div>
                    </div>
                </div>
                <!-- col end -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-box rounded mt-4 p-4">
                        <div class="service-icon mb-3">
                            <i class="mdi mdi-yin-yang"></i>
                        </div>
                        <div class="services-desc">
                            <div class="service-title mb-2 position-relative">
                                <h5 class="font-weight-normal mb-3"><a href="#" class="text-dark">Digital Design</a></h5>
                            </div>
                            <p class="text-muted mb-3 f-14">Sed ut perspiciatis unde sit omnise iste natus voluptatem site accusantium doloremque laudantium totam.</p>
                            <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">learn more<i class="mdi mdi-arrow-right ml-2"></i></a></p>
                        </div>
                    </div>
                </div>
                <!-- col end -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-box rounded mt-4 p-4">
                        <div class="service-icon mb-3">
                            <i class="mdi mdi-apple-keyboard-command"></i>
                        </div>
                        <div class="services-desc">
                            <div class="service-title mb-2 position-relative">
                                <h5 class="font-weight-normal mb-3"><a href="#" class="text-dark">Easy to customize</a></h5>
                            </div>
                            <p class="text-muted mb-3 f-14">Sed ut perspiciatis unde sit omnise iste natus voluptatem site accusantium doloremque laudantium totam.</p>
                            <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">learn more<i class="mdi mdi-arrow-right ml-2"></i></a></p>
                        </div>
                    </div>
                </div>
                <!-- col end -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-box rounded mt-4 p-4">
                        <div class="service-icon mb-3">
                            <i class="mdi mdi-hexagon-multiple"></i>
                        </div>
                        <div class="services-desc">
                            <div class="service-title mb-2 position-relative">
                                <h5 class="font-weight-normal mb-3"><a href="#" class="text-dark">Truly Multipurpose</a></h5>
                            </div>
                            <p class="text-muted mb-3 f-14">Sed ut perspiciatis unde sit omnise iste natus voluptatem site accusantium doloremque laudantium totam.</p>
                            <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">learn more<i class="mdi mdi-arrow-right ml-2"></i></a></p>
                        </div>
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- SERVICE END -->

    <!-- FEATURES START -->
    <section class="section bg-features bg-light" id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-heading mb-5">
                        <h3 class="text-dark mb-1 font-weight-light text-uppercase">Our Features</h3>
                        <div class="title-border-simple position-relative"></div>
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->

            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="features-content">
                        <div class="features-icon">
                            <i class="pe-7s-science text-danger"></i>
                        </div>
                        <h4 class="font-weight-normal text-dark mb-3 mt-4">Tersedia di Semua Platform</h4>
                        {{-- Avalaible in all platform --}}
                        <p class="text-muted f-14">Produk kami bersifat responsive yang berarti bisa digunakan untuk semua platform dari smartphone hingga komputer.</p>
                        {{-- Our product is responsive, it can be accesses  by any platform from smartphone to pc.--}}
                        <p class="text-muted f-14">Hal tersebut dapat memudahkan kalian semua untuk mengakses atau menggunakan produk kami tanpa memikirkan keterbatasan perangkat.</p>
                        {{-- This can make it easier for all of you to access or use our products without device limitations. --}}
                        <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">Pelajari lebih lanjut <span class="right-arrow ml-1">&#x21FE;</span></a></p>
                        {{-- Learn more --}}
                    </div>
                </div>
                <!-- col end -->
                <div class="col-lg-6">
                    <div class="features-img mt-32">
                        <img src="{{ asset('landing/images/features-img/img-1.png') }}" alt="" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- FEARURES 1 END -->

    <section class="section bg-features">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="features-img">
                        <img src="{{ asset('landing/images/features-img/img-2.png') }}" alt="" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <!-- col end -->
                <div class="col-lg-6">
                    <div class="features-content mt-32">
                        <div class="features-icon">
                            <i class="pe-7s-shuffle text-danger"></i>
                        </div>
                        <h4 class="font-weight-normal text-dark mb-3 mt-4">Search Box</h4>
                        {{-- Searc Box --}}
                        <p class="text-muted f-14">Produk kami tersedia dengan fitur search box, yang berarti anda bisa mencari semua majalah atau artikel yang anda inginkan dengan mudah.</p>
                        {{-- Our product also come with search box.Which can make you find the magazine you are looking for easily --}}
                        <p class="text-muted f-14">Ini bisa memudahkan kalian mencari majalah yang kalian inginkan tanpa harus mem scroll ke semua halaman.</p>
                        {{-- This can make you look up for things to read without scrolling through all pages--}}
                        <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">Pelajari lebih lanjut <span class="right-arrow ml-1">&#x21FE;</span></a></p>
                        {{-- Learn More --}}
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- FEARURES 2 END -->

    <section class="section bg-features bg-light">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="features-content">
                        <div class="features-icon">
                            <i class="pe-7s-display1 text-danger"></i>
                        </div>
                        <h4 class="font-weight-normal text-dark mb-3 mt-4">Favorit</h4>
                        {{-- Favorite --}}
                        <p class="text-muted f-14">Kami juga menyediakan fitur favorit pada produk kami, yang berguna untuk menandai page yang anda suka.</p>
                        {{-- We also provide a favorite feature on our products, which allows you to tag the page that you like --}}
                        <p class="text-muted f-14">Fitur ini dapat memudahkan kalian untuk kembali ke page yang anda inginkan dengan mudah.</p>
                        {{-- This feature allow you to access the page that you want with ease. --}}
                        <p class="mb-0 text-uppercase f-13"><a href="#" class="text-primary">Pelajari lebih lanjut <span class="right-arrow ml-1">&#x21FE;</span></a></p>
                        {{-- Learn more --}}
                    </div>
                </div>
                <!-- col end -->
                <div class="col-lg-6">
                    <div class="features-img mt-40">
                        <img src="{{ asset('landing/images/features-img/img-3.png') }}" alt="" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- FEATURES 3 END -->

    <!-- FOOTER START -->
    <section class="footer-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="mb-5">
                        <p class="text-uppercase text-dark footer-title mb-4">Tentang Kami</p>
                        {{-- About Us --}}
                        <p class="text-muted f-14">Tim Programming SMKN 1 Panji.</p>
                        {{-- Programming Team SMKN 1 Panji. --}}
                    </div>
                </div>
                <!-- col end -->
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="mb-5">
                                <p class="text-uppercase text-dark footer-title mb-4">company</p>
                                <ul class="list-unstyled footer-sub-menu">
                                    <li class="f-14"><a href="" class="text-muted">Monitoring Grader</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Job Opening</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Customers</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Privacy</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- col end -->

                        <div class="col-lg-4">
                            <div class="mb-5">
                                <p class="text-uppercase text-dark footer-title mb-4">support</p>
                                <ul class="list-unstyled footer-sub-menu">
                                    <li class="f-14"><a href="" class="text-muted">Get Started</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Blog</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Knowledge Base</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- col end -->

                        <div class="col-lg-4">
                            <div class="mb-5">
                                <p class="text-uppercase text-dark footer-title mb-4">Legal</p>
                                <ul class="list-unstyled footer-sub-menu">
                                    <li class="f-14"><a href="" class="text-muted">Terms & Conditions</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Privacy Policy</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Customers</a></li>
                                    <li class="f-14"><a href="" class="text-muted">Pricing</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- col end -->
                    </div>
                    <!-- row end -->
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- FOOTER END -->

    <!-- FOOTER ALT START -->
    <section class="footer-alt bg-dark pt-3 pb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="copyright text-white f-14 font-weight-light mb-0 text-uppercase">{{ date('Y') }} © {{ App\Models\Setting::title() }}</p>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- FOOTER ALT END -->

    <script src="{{ ('landing/js/jquery.min.js') }}"></script>
    <script src="{{ ('landing/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ ('landing/js/scrollspy.min.js') }}"></script>
    <script src="{{ ('landing/js/jquery.easing.min.js') }}"></script>
    <script src="{{ ('landing/js/anime.min.js') }}"></script>
    <!-- carousel -->
    <script src="{{ ('landing/js/owl.carousel.min.js') }}"></script>
    <!-- Main Js -->
    <script src="{{ ('landing/js/app.js') }}"></script>

    <script>
        //owlCarousel
        $(document).ready(function() {

            $("#owl-demo").owlCarousel({
                autoPlay: 3000, //Set AutoPlay to 3 seconds

                items: 2,
                itemsDesktop: [1199, 2],
                itemsDesktopSmall: [979, 2]

            });
        });
    </script>

</body>
</html>
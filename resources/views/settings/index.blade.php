@extends('layouts.app')

@section('title', 'Settings')

@section('content')
<div class="row justify-content-center">
    <div class="col-12">
        <div class="row align-items-center mb-2">
            <div class="col">
                <h2 class="h5 page-title">@yield('title')</h2>
                <p class="text-muted">Pengaturan website</p>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="card shadow mb-4">
                    <div class="card-header">
                        <strong class="card-title">@yield('title')</strong>
                    </div>
                    <div class="card-body px-4">
                        <form action="{{ route('admin.settings.update') }}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-6">
                                    @foreach ($settings as $setting)
                                        @if ($setting->type == 'text')
                                            <div class="form-group">
                                                <label class="text-capitalize" for="{{ $setting->variable }}">{{ $setting->variable }}</label>
                                                <input type="text" name="{{ $setting->variable }}" id="{{ $setting->variable }}"
                                                class="form-control @error($setting->variable) is-invalid @enderror"
                                                value="{{ $setting->value }}">
                                                
                                                @error($setting->variable)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong class="text-danger">{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        @else
                                            <div class="form-group">
                                                <label class="text-capitalize" for="{{ $setting->variable }}">{{ str_replace('-', ' ', $setting->variable) }}</label>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control @error($setting->variable) is-invalid @enderror" name="{{ $setting->variable }}" id="{{ $setting->variable }}" value="{{ $setting->value }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="{{ $setting->variable }}" onclick="filemanager.selectFile('{{ $setting->variable }}')" style="cursor: pointer">Pilih</span>
                                                    </div>
                                                    
                                                    @error($setting->variable)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong class="text-danger">{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                    <button type="submit" class="btn btn-outline-primary float-right fe fe-send fe-16">
                                        Simpan Perubahan</button>
                                </div>
                            </div>
                        </form>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.auth')

@section('title', 'Login')

@section('content')
<form class="col-lg-3 col-md-4 col-10 mx-auto text-center" method="POST" action="{{ route('login') }}">
    <a class="navbar-brand mx-auto pb-3 mb-4 flex-fill text-center" href="{{ route('login') }}">
        <img src="{{ asset('admin/images/logo.png') }}" id="logo" class="navbar-brand-img brand-md">
    </a>
    @csrf
    <div class="form-group">
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
            value="{{ old('email') }}" placeholder="E-mail" required autocomplete="email" autofocus>

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong class="text-danger">{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
            name="password" placeholder="Password" required autocomplete="current-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong class="text-danger">{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <hr>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
</form>
@endsection

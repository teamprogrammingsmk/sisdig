<script src="{{ asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ asset('admin/js/popper.min.js') }}"></script>
<script src="{{ asset('admin/js/moment.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/js/simplebar.min.js') }}"></script>
<script src='{{ asset('admin/js/daterangepicker.js') }}'></script>
<script src='{{ asset('admin/js/jquery.stickOnScroll.js') }}'></script>
<script src="{{ asset('admin/js/tinycolor-min.js') }}"></script>
<script src="{{ asset('admin/js/config.js') }}"></script>

<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('datatables/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/select2/select2.min.js') }}"></script>

<script src="{{ asset('admin/js/apps.js') }}"></script>
<script src="{{ asset('admin/js/customs.js') }}"></script>

@yield('script')

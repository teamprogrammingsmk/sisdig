<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="{{ App\Models\Setting::description() }}">
<meta name="author" content="">
<link rel="icon" href="{{ asset('admin/images/favicon.ico') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ App\Models\Setting::title() }} - @yield('title')</title>
<!-- Simple bar CSS -->
<link rel="stylesheet" href="{{ asset('admin/css/simplebar.css') }}">
<!-- Fonts CSS -->
<link
    href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet">
<!-- Icons CSS -->
<link rel="stylesheet" href="{{ asset('admin/css/feather.css') }}">
<!-- Date Range Picker CSS -->
<link rel="stylesheet" href="{{ asset('admin/css/daterangepicker.css') }}">

<link type="text/css" href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('datatables/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">

<link type="text/css" href="{{ asset('admin/select2/select2.min.css') }}" rel="stylesheet">
<!-- App CSS -->
<link rel="stylesheet" href="{{ asset('admin/css/app-light.css') }}" id="lightTheme">
<link rel="stylesheet" href="{{ asset('admin/css/app-dark.css') }}" id="darkTheme" disabled>

@FilemanagerScript

@yield('css')

<aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
    <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
        <i class="fe fe-x"><span class="sr-only"></span></i>
    </a>
    <nav class="vertnav navbar navbar-light">
        <!-- nav bar -->
        <div class="w-100 mb-2 d-flex">
            <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="./index.html">
                <img src="{{ asset('admin/images/logo.png') }}" id="logo" class="navbar-brand-img brand-sm">
            </a>
        </div>
        <p class="text-muted nav-heading mb-1">
            <span>Navigasi</span>
        </p>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item dropdown">
                <a href="{{ route('admin.home') }}" class="nav-link">
                    <i class="fe fe-home fe-16"></i>
                    <span class="ml-3 item-text">Beranda</span>
                </a>
                <a href="#portfolio" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                    <i class="fe fe-book fe-16"></i>
                    <span class="ml-3 item-text">Post</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100" id="portfolio">
                    <li class="nav-item">
                        <a class="nav-link pl-3" href="{{ route('admin.article.index') }}"><span
                                class="ml-1 item-text">Semua Post</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-3" href="{{ route('admin.article.create') }}"><span
                                class="ml-1 item-text">Tambah Baru</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pl-3" href="{{ route('admin.category.index') }}"><span
                                class="ml-1 item-text">Kategori</span></a>
                    </li>
                </ul>
                <a href="javascript::void(0);" target="popup" onclick="window.open('{{ url('admin/filemanager') }}','popup','width=600,height=600'); return false;" class="nav-link">
                    <i class="fe fe-folder fe-16"></i>
                    <span class="ml-3 item-text">Filemanager</span>
                </a>
            </li>
        </ul>
        <p class="text-muted nav-heading mb-1">
            <span>Settings</span>
        </p>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item dropdown">
                <a href="{{ route('admin.settings.index') }}" class="nav-link">
                    <i class="fe fe-settings fe-16"></i>
                    <span class="ml-3 item-text">Settings</span>
                </a>
            </li>
        </ul>
    </nav>
</aside>
